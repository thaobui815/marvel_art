<?php include "./header.php"; ?>
<main class="main-sigin creat-account photo-bg" style="background-image: url(./assets/images/account/account-bg.png);">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
			</div>
			<div class="col-md-6">
				<div class="inner">
					<div class="title-general">Sign in using your email</div>
					<div class="signin-form--wrap">
						<form>
							<div class="form-group">
								<input type="email" class="form-control" placeholder="Enter email">
							</div>
							<div class="form-group">
								<input type="password" class="form-control" placeholder="Password">
							</div>
							<div class="form-group">
								<a href="#">Forgot your password</a>
							</div>
							<button type="submit" class="btn btn-primary">Sign in</button>
							<div class="already text-center">
								Don't have an account? &nbsp;<a href="#">Forgot your password</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include "./footer.php"; ?>
</html>
</body>
<script> 
	jQuery(document).ready(function($) {
		$('.js-header').addClass('is-page');
	});
</script>

