<?php include "./header.php"; ?>
<main class="main-sigin pd-5 cate-detail" style="background-color: #fff;">
    <section class="page-banner">
        <img src="https://picsum.photos/1920/400" height="400" alt="#" class="img-fluid">
        <div class="text-banner position-absolute">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
    </section>
    <div class="container pt-5 pb-5">
        <div class="row mb-5">
            <div class="col-md-7">
                <div class="swiper-container gallery-top">
                    <div class="swiper-wrapper">
                        <?php for ($i = 1; $i <= 8; $i++) : ?> 
                            <div class="swiper-slide">
                                <div class="photo-bg" style="background-image:url(https://picsum.photos/900/600)"></div>
                                <a href="https://picsum.photos/900/600" data-fancybox="group_1" class="mdi mdi-arrow-expand-all position-absolute"></a>
                            </div>
                        <?php endfor; ?> 
                    </div>
                </div>
                <div class="small-slide" style="position: relative;">
                    <div class="swiper-container gallery-thumbs mb-5 p-0">
                        <div class="swiper-wrapper">
                            <?php for ($i = 1; $i <= 8; $i++) : ?> 
                                <div class="swiper-slide">
                                    <div class="photo-bg" style="background-image:url(https://picsum.photos/900/600)"></div>
                                </div>
                            <?php endfor; ?> 
                        </div>
                    </div>
                    <div class="swiper-button-next"><span class="mdi mdi-chevron-right position-absolute"></span></div>
                    <div class="swiper-button-prev"><span class="mdi mdi-chevron-left position-absolute"></span></div>
                </div>
            </div>
            <div class="col-md-5">
                <h1 class="title-cate-detail border-0 text-uppercase">SOVE CHARMBRAY LINEN EURO PILLOWCASE</h1>
                <div class="price text-uppercase">$29.94</div>
                <div class="quantity d-flex mb-2 align-items-center">
                    <span class="d-inline-block">Quantitty</span>
                    <div class="form-group mb-0">
                        <span class="btn-down mdi mdi-minus"></span>
                        <input class="text-center" type="text" value="1">
                        <span class="btn-up mdi mdi-plus"></span>
                    </div>
                </div>
                <div class="mb-4">
                    <div>Colour: Charcoal</div>
                    <div class="d-flex mt-1">
                        <label class="c-checkbox mr-3">
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>
                        <label class="c-checkbox mr-3">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <label class="c-checkbox mr-3">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
                <div class="mb-2">Size: 65x65cm</div>
                <div class="mb-3">Sku: SRT0002EU</div>
                <div class="d-flex align-items-center mb-3">
                    <button type="button" class="btn shop-now text-uppercase mt-0">Add to card</button>
                    <a href="#" class="ml-3"><span class="icon-heart mdi mdi-heart"></span></a>
                </div>
                <div class="mb-3">The Sove 100% Chambray Linen Euro Pillowcase in rich navy tones looks crisp and inviting all year round. Hunker down and rediscover the abundant joys of sleeping in.</div>
                <div class="accordion" id="accordionExample">
                    <div class="card position-relative border-left-0 border-right-0 border-top-0">
                        <div class="card-header p-0" id="headingOne">
                            <h2 class="mb-0 position-relative">
                                <button class="btn btn-link w-100 text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                   Dimensions
                                </button>
                                <span class="position-absolute more down"></span>
                            </h2>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                Anim pariatur cliche Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias saepe vero ex, totam quas, nostrum repellendus possimus dolores suscipit? Ratione, at officia accusamus inventore eos, impedit ut soluta fuga cupiditate.
                            </div>
                        </div>
                    </div>
                    <div class="card position-relative border-left-0 border-right-0">
                        <div class="card-header p-0" id="headingTwo">
                            <h2 class="mb-0 position-relative">
                                <button class="btn btn-link w-100 text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Item #2
                                </button>
                                <span class="position-absolute more"></span>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                Anim pariatur cliche Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet, ex quis atque ab est corporis officiis perspiciatis obcaecati? Reprehenderit ex eligendi laborum aliquid, obcaecati voluptatum repellat ad, id molestiae vitae?
                            </div>
                        </div>
                    </div>
                    <div class="card position-relative border-left-0 border-right-0">
                        <div class="card-header p-0" id="headingThree">
                            <h2 class="mb-0 position-relative">
                                <button class="btn btn-link w-100 text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Item #3
                                </button>
                                <span class="position-absolute more"></span>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                Anim pariatur cliche Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed numquam hic nobis! Sint accusamus sapiente excepturi debitis corporis similique ex ut, reprehenderit saepe doloribus, modi quasi amet facilis consequuntur molestiae.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            jQuery(document).ready(function($) {
                $('.card-header .btn.btn-link').click(function(event) {
                    $(this).parent().find('.mdi-plus').removeClass('mdi-plus').addClass('mdi-minus');
                });
                $('.card-header .btn.btn-link').click(function(event) {
                    $(this).parent().find('.mdi-minus').removeClass('mdi-minus').addClass('mdi-plus');
                });
            });
        </script>
        <h4 class="pb-3 title-cate text-uppercase font-weight-bold">Recommended for you</h4>
        <div class="row mb-5">
            <?php 
            for ($i = 0; $i < 4; $i++) :
                ?>
                <div class="col-md-3 mb-3">
                    <div class="card border-0">
                        <span class="icon-heart"></span>
                        <a href="" style="background-image: url(https://picsum.photos/900/600);" class="card-img-top" ></a>
                        <div class="card-body pl-0 pr-0">
                            <a href="" class="text-uppercase"><h6 class="card-title text-uppercase">wilfred the wale</h6></a>
                            <p class="card-text"><span class="text-uppercase font-weight-bold"><strong>$59.90</strong></span></p>
                        </div>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
        <h4 class="pb-3 title-cate text-uppercase font-weight-bold">Recently viewed products</h4>
        <div class="row">
            <?php 
            for ($i = 0; $i < 4; $i++) :
                ?>
                <div class="col-md-3 mb-3">
                    <div class="card border-0">
                        <span class="icon-heart"></span>
                        <a href="" style="background-image: url(https://picsum.photos/900/600);" class="card-img-top" ></a>
                        <div class="card-body pl-0 pr-0">
                            <a href=""><h6 class="card-title text-uppercase">wilfred the wale</h6></a>
                            <p class="card-text"><span><strong>$59.90</strong></span></p>
                        </div>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
    </div>
</main>
<?php include "./footer.php"; ?>
</html>
</body>
<script> 
    jQuery(document).ready(function($) {
        $('.js-header').addClass('is-page');
        $('.btn-up').click(function(event) {
            var value_up = $(this).prev().attr('value');
            value_up = Number(value_up);
            values_up = value_up+1;
            $(this).prev().attr('value', values_up);
        });
        $('.btn-down').click(function(event) {
            var value_down = $(this).next().attr('value');
            value_down = Number(value_down);
            if(value_down == 0){
                $(this).next().attr('value', '0');
            }
            else{
                values_down = value_down-1;
                $(this).next().attr('value', values_down);
            }
        });
        $('.card-header h2').click(function(event) {
            $('.card-header h2 .down').removeClass('down');
            $(this).children('.more').toggleClass('down');
        });
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 50,
        slidesPerView: 3,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.small-slide .swiper-button-next',
            prevEl: '.small-slide .swiper-button-prev',
        },
        autoplay: {
            delay: 5000,
        },
        breakpoints: {
            1440: {
                spaceBetween: 30,
            },
            480: {
                spaceBetween: 10,
                autoplay: {
                    delay: 1000,
                },
            },
        }
    });
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 0,
        thumbs: {
            swiper: galleryThumbs
        },
    });
</script>
<style>
    .swiper-container {
        width: 100%;
        height: auto;
        margin-left: auto;
        margin-right: auto;
    }
    .swiper-slide {
        background-size: cover;
        background-position: center;
    }
    .gallery-thumbs {
        height: auto;
        box-sizing: border-box;
        padding: 10px 0;
    }
    .gallery-thumbs .swiper-slide {
        height: auto;
    }
    .gallery-thumbs .swiper-slide-thumb-active {
        opacity: 1;
    }
</style>