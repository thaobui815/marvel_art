<?php include "./header.php"; ?>
<main class="main-sigin creat-account photo-bg" style="background-image: url(./assets/images/account/account-bg.png);">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
			</div>
			<div class="col-md-6">
				<div class="inner">
					<div class="title-general border-0">Create an account</div>
					<div class="signin-form--wrap">
						<form>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control bg-transparent" placeholder="First name*">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control bg-transparent" placeholder="Last name*">
									</div>
								</div>
							</div>
							<div class="form-group">
								<input type="email" class="form-control bg-transparent" placeholder="Email*">
							</div>
							<div class="form-group">
								<input type="password" class="form-control bg-transparent" placeholder="Password*">
							</div>
							<div class="form-group">
								<input type="password" class="form-control bg-transparent" placeholder="Confirm Password*">
							</div>
							<div class="form-group form-check d-flex align-items-center">
								<label class="c-checkbox m-0">
									<input type="checkbox" class="form-check-input" id="exampleCheck1">
									<span class="checkmark"></span>
								</label>
								<label class="form-check-label" for="exampleCheck1">Subscribe to our newsletter</label>
							</div>
							<div class="form-group form-check d-flex align-items-center">
								<label class="c-checkbox m-0">
									<input type="checkbox" class="form-check-input" id="exampleCheck2">
									<span class="checkmark"></span>
								</label>
								<label class="form-check-label" for="exampleCheck2">I accept the <a href="#">Privacy Policy</a></label>
							</div>
							<button type="submit" class="btn text-white font-weight-bold rounded-0 border-0 w-100">Create</button>
							<div class="form-group text-center already">
								Already have an account? &nbsp;<a href="#">Sign in</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include "./footer.php"; ?>
</html>
</body>
<script> 
	jQuery(document).ready(function($) {
		$('.js-header').addClass('is-page');
	});
</script>

