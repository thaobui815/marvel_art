<?php include "./header.php"; ?>
<main class="main-sigin pd-5 category" style="background-color: #fff;">
    <section class="page-banner">
        <img src="./assets/images/cates/667-1920x400.jpg" height="400" alt="#" class="img-fluid">
        <div class="text-banner position-absolute">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="filter-group mb-5">
                    <h5 class="pb-2 mb-4 title-general w-100 w-100">Categories</h5>
                    <div class="accordion" id="accordionExample">
                        <div class="card position-relative border-left-0 border-right-0 border-top-0">
                            <div class="card-header p-0" id="headingOne">
                                <h2 class="mb-0 position-relative">
                                    <button class="btn btn-link w-100 text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Group Item #1
                                    </button>
                                    <span class="position-absolute more down"></span>
                                </h2>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    Anim pariatur cliche
                                </div>
                            </div>
                        </div>
                        <div class="card position-relative border-left-0 border-right-0">
                            <div class="card-header p-0" id="headingTwo">
                                <h2 class="mb-0 position-relative">
                                    <button class="btn btn-link w-100 text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Group Item #2
                                    </button>
                                    <span class="position-absolute more"></span>
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    Anim pariatur cliche
                                </div>
                            </div>
                        </div>
                        <div class="card position-relative border-left-0 border-right-0">
                            <div class="card-header p-0" id="headingThree">
                                <h2 class="mb-0 position-relative">
                                    <button class="btn btn-link w-100 text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Group Item #3
                                    </button>
                                    <span class="position-absolute more"></span>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    Anim pariatur cliche
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="filter-group mb-5">
                    <h5 class="pb-2 mb-4 title-general w-100">Color</h5>
                    <div class="row m-0">
                        <div class="col-md-6">
                            <label class="c-checkbox d-flex align-items-center h-auto text-dark">Black
                                <input type="checkbox" checked="checked">
                                <span class="checkmark border-0" style="background-color: black;"></span>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="c-checkbox d-flex align-items-center h-auto text-dark">Red
                                <input type="checkbox" checked="checked">
                                <span class="checkmark border-0" style="background-color: red;"></span>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="c-checkbox d-flex align-items-center h-auto text-dark">Yellow
                                <input type="checkbox" checked="checked">
                                <span class="checkmark border-0" style="background-color: yellow;"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="filter-group mb-5">
                    <h5 class="pb-2 mb-4 title-general w-100">Size</h5>
                    <div class="row m-0">
                        <div class="col-md-6">
                            <label class="c-checkbox d-flex align-items-center h-auto text-dark">L
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="c-checkbox d-flex align-items-center h-auto text-dark">M
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="c-checkbox d-flex align-items-center h-auto text-dark">On size
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="c-checkbox d-flex align-items-center h-auto text-dark">S
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="filter-group mb-5">
                    <h5 class="pb-2 mb-4 title-general w-100">Availability</h5>
                    <div class="row m-0">
                        <div class="col-12">
                            <label class="c-checkbox d-flex align-items-center h-auto text-dark">Availability
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="c-checkbox d-flex align-items-center h-auto text-dark">Sale off
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="c-checkbox d-flex align-items-center h-auto text-dark">Comming Soon
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="filter-group mb-5">
                    <h5 class="pb-2 mb-4 title-general w-100">Price</h5>
                    <div class="row m-0">
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row mb-4">
                    <div class="col-md-6 brcrumb">
                        <h6 class="m-0 pt-2 font-weight-bold">Shop / All Product</h6>
                    </div>
                    <div class="col-md-6 d-flex justify-content-end">
                        <div class="dropdown pr-3">
                            <button class="btn mdi mdi-chevron-down d-flex flex-row-reverse align-items-center" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Sort by
                            </button>
                            <div class="dropdown-menu border-0" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Price (Low to High)</a>
                                <a class="dropdown-item" href="#">Price (High to Low)</a>
                                <a class="dropdown-item" href="#">Product Name</a>
                                <a class="dropdown-item" href="#">Newest</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="card position-relative border-0">
                            <a href="" style="background-image: url(./assets/images/cates/603-900x600.jpg)"  class="card-img-top  photo-bg w-100 d-inline-block" alt="Card title"></a>
                            <div class="interactive position-absolute w-100 d-flex justify-content-center align-items-center">
                                <a href="" class="mdi mdi-cart-outline d-inline-block"></a>
                                <a href="" class="mdi mdi-heart-outline d-inline-block" data-toggle="tooltip" data-placement="top" title="66"></a>
                                <a href="" class="mdi mdi-eye-outline d-inline-block" data-toggle="tooltip" data-placement="top" title="88"></a>
                            </div>
                            <div class="card-body">
                                <a href=""><h6 class="card-title text-uppercase">wilfred the wale </h6></a>
                                <p class="card-text">
                                    <span class="text-uppercase style-price-old text-secondary mr-2">$99.90</span>
                                    <span class="text-warning"><strong>$59.90</strong></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php 
                    for ($i = 0; $i < 8; $i++) :
                        ?>
                        <div class="col-md-4 mb-3">
                            <div class="card position-relative border-0">
                                <a href="" style="background-image: url(./assets/images/cates/603-900x600.jpg)"  class="card-img-top  photo-bg w-100 d-inline-block" alt="Card title"></a>
                                <div class="interactive position-absolute w-100 d-flex justify-content-center align-items-center">
                                    <a href="" class="mdi mdi-cart-outline d-inline-block"></a>
                                    <a href="" class="mdi mdi-heart-outline d-inline-block" data-toggle="tooltip" data-placement="top" title="66"></a>
                                    <a href="" class="mdi mdi-eye-outline d-inline-block" data-toggle="tooltip" data-placement="top" title="88"></a>
                                </div>
                                <div class="card-body">
                                    <a href=""><h6 class="card-title text-uppercase">wilfred the wale</h6></a>
                                    <p class="card-text">
                                        <span><strong>$59.90</strong></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
                        <li class="page-item text-dark font-weight-bold border-0 p-0 d-flex justify-content-center align-items-center"><a class="page-link" href="#"><span class="icon-heart mdi mdi-menu-left text-dark"></span></a></li>
                        <li class="page-item text-dark font-weight-bold border-0 p-0 d-flex justify-content-center align-items-center"><a class="page-link" href="#">1</a></li>
                        <li class="page-item text-dark font-weight-bold border-0 p-0 d-flex justify-content-center align-items-center"><a class="page-link" href="#">2</a></li>
                        <li class="page-item text-dark font-weight-bold border-0 p-0 d-flex justify-content-center align-items-center"><a class="page-link" href="#">3</a></li>
                        <li class="page-item text-dark font-weight-bold border-0 p-0 d-flex justify-content-center align-items-center"><a class="page-link" href="#"><span class="icon-heart mdi mdi-menu-right text-dark"></span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</main>
<?php include "./footer.php"; ?>
</html>
</body>
<script> 
    jQuery(document).ready(function($) {
        $('.js-header').addClass('is-page');
        $('.card-header h2').click(function(event) {
            $('.card-header h2 .down').removeClass('down');
            $(this).children('.more').toggleClass('down');
        });
    });
</script>