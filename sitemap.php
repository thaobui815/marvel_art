<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sitemaps</title>
</head>
<body>
    <p>Sitemaps</p>
    <ul>
        <li><a target="_blank" href="./index.php">Home</a></li>
        <li><a target="_blank" href="./categories.php">Categories</a></li>
        <li><a target="_blank" href="./categories-detail.php">Categories detail</a></li>
        <li><a target="_blank" href="./signin.php">Signin</a></li>
        <li><a target="_blank" href="./create-account.php">Create account</a></li>
        <li><a target="_blank" href="./forget-password.php">Forget password</a></li>
        <li><a target="_blank" href="./account-history.php">Account history</a></li>
        <li><a target="_blank" href="./account-favorites.php">Account favorites</a></li>
        <li><a target="_blank" href="./account-detail.php">Account detail</a></li>
    </ul>
</body>
</html>

