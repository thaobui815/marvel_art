<section class="section-5">
	<div class="_title_container">WHAT PEOPLE SAY <br> ABOUT US</div>
	<div class="wrap_slide_s5">
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="_item">
						<div class="_left">
							<div class="_content">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap i electronic typesetting, remaining essentially unchanged. </div>
							<div class="_name">JANE DOE</div>
							<div class="_position">Designer at 123</div>
						</div>
						<div class="_right">
							<div class="_avatar" style="background-image: url('./assets/images/home_q/avata.png');"></div>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="_item">
						<div class="_left">
							<div class="_content">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap i electronic typesetting, remaining essentially unchanged. </div>
							<div class="_name">JANE DOE</div>
							<div class="_position">Designer at 123</div>
						</div>
						<div class="_right">
							<div class="_avatar" style="background-image: url('./assets/images/home_q/avata.png');"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="swiper-button-next">
			<img src="./assets/images/home_q/next.png" alt="">
		</div>
    	<div class="swiper-button-prev">
    		<img src="./assets/images/home_q/prev.png" alt="">
    	</div>
	</div>
</section>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		var slideS5 = new Swiper('.wrap_slide_s5 .swiper-container',{
			speed: 1000,
			spaceBetween: 10,
			// autoplay:{
			// 	delay: 3000
			// },
			navigation: {
		        nextEl: '.wrap_slide_s5 .swiper-button-next',
		        prevEl: '.wrap_slide_s5 .swiper-button-prev',
	        },
		});
	});
</script>
