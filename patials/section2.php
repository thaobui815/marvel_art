<section class="section-2">
	<?php for ($i = 1; $i <= 5; $i++) : ?> 
		<div class="item-block --pc">
			<div class="photo-bg" style="background-image: url(./assets/images/homepage/s2-<?php echo $i; ?>.png);"></div>
			<div class="desc">
				<h3>furniture</h3>
				<p>Gallery</p>
			</div>
		</div>
	<?php endfor; ?> 
	<div class="swiper-container --mobile">
		<div class="swiper-wrapper">
			<?php for ($i = 1; $i <= 5; $i++) : ?> 
				<div class="swiper-slide item-block">
					<div class="photo-bg" style="background-image: url(./assets/images/homepage/s2-<?php echo $i; ?>.png);"></div>
					<div class="desc">
						<h3>furniture</h3>
						<p>Gallery</p>
					</div>
				</div>
			<?php endfor; ?> 
		</div>
	</div>
</section>

<script>
	var swiper = new Swiper('.section-2 .swiper-container', {
		autoplay:{
			delay: 3000
		},
		speed: 1000,
		loop: true,
		effect: 'fade,'
	});
	jQuery(document).ready(function($) {
	});
</script>