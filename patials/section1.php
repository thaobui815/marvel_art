<section class="section-1">
	<div class="swiper-container">
		<div class="swiper-wrapper">
			<div class="swiper-slide photo-bg" style="background-image: url(./assets/images/homepage/s1.png);">
				<div class="text-block">
					<h2>new collection 2019</h2>
					<h3>sale up to 50% off</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever </p>
					<a href="#" class="shop-now">shop now</a>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	jQuery(document).ready(function($) {
		var swiper = new Swiper('.section-1 .swiper-container',{
			autoplay:{
				delay: 3000
			},
			allowTouchMove: false,
		});
	});
</script>