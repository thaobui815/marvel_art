<section class="section-3">
	<h2 class="title-general border-bottom-0">featured products</h2>
	<div class="product">
		<?php for ($i = 1; $i <= 8; $i++) : ?> 
			<a href="" class="_1item">
				<div class="photo-bg" style="background-image: url(./assets/images/homepage/s3-<?php echo $i ?>.png);"></div>
				<div class="detail">
					<div class="name">Sofas & Armchairs</div>
					<div class="price">$690.00</div>
				</div>
			</a>
		<?php endfor; ?> 
	</div>
</section>

<script>
	jQuery(document).ready(function($) {
	});
</script>