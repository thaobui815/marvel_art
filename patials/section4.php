<section class="section-4">
	<div class="_item">
		<div class="_img" style="background-image: url('./assets/images/home_q/s4.png');"></div>
		<div class="_text">
			<div class="_title">NEW COLLECTION FURNITURE</div>
			<div class="_sub_title">Lorem ipsum dolor sit amet, consectetuer</div>
			<a href="#">Shop now</a>
		</div>
	</div>
	<div class="_item">
		<div class="_img" style="background-image: url('./assets/images/home_q/s42.png');"></div>
		<div class="_text">
			<div class="_title">KITCHEN SALE UP TO 30% OFF</div>
			<div class="_sub_title">Serving plates + Bowls + Stands + Coffee cup </div>
			<a href="#">Shop now</a>
		</div>
	</div>
	<div class="_item">
		<div class="_img" style="background-image: url('./assets/images/home_q/s43.png');"></div>
		<div class="_text">
			<div class="_title">FREE SHIPPING ON BATH</div>
			<div class="_sub_title">Discover our exclusive towels, decor, hardware + more</div>
			<a href="#">Shop now</a>
		</div>
	</div>
</section>

<script>
	jQuery(document).ready(function($) {
	});
</script>