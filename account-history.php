<?php include "./header.php"; ?>
<main class="main-sigin pd-5 history">
	<section class="page-banner">
		<img src="https://picsum.photos/1920/400" height="400" alt="#" class="img-fluid">
        <div class="text-banner position-absolute">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
	</section>
	<div class="container pt-5 pb-5">
		<div class="row m-md-0">
			<div class="menu-tab col-xl-3 col-md-3">
				<div class="pb-2 title-general border-0">Explore Your Account</div>
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link" href="#">My Detail</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="#">Favorites</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Order History</a>
					</li>
				</ul>
			</div>
			<div class="input-info col-xl-9 col-md-9">
				<h3 class="pb-3">My order</h3>
				<table class="table">
					<thead>
						<tr>
							<th class="text-left pl-0 border-top-0">Product</th>
							<th class="text-center border-top-0">Code orders</th>
							<th class="text-center border-top-0">Total</th>
							<th class="text-left border-top-0">Status order</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						for ($i=0; $i<5; $i++):
							?>
							<tr>
								<td class="text-center p-0 text-uppercase border-0 pl-0 border-top-0"><small class="text-capitalize">27/03/2019</small></td>
								<td class="text-center p-0 text-uppercase border-0 border-top-0 pb-0">&nbsp;</td>
								<td class="text-center p-0 text-uppercase border-0 border-top-0 pb-0">&nbsp;</td>
								<td class="text-center p-0 text-uppercase border-0 border-top-0 pr-0 pb-0">&nbsp;</td>
							</tr>
							<tr>
								<td class="text-center p-0 text-uppercase border-0 pl-0 border-bottom-0">
									<div class="card bg-transparent border-0" style="max-width: 540px;">
										<div class="row no-gutters">
											<div class="col-md-4 col-sm-3 card-img photo-bg" style="background-image: url(https://picsum.photos/120/120);"></div>
											<div class="col-md-8 col-sm-9">
												<div class="card-body h-100 d-flex flex-column justify-content-between">
													<h5 class="card-title border-bottom-0 mb-2">Sove Chambray Linen Euro Pillowcase</h5>
													<p class="card-text mb-1"><small class="text-muted">x1</small></p>
													<p class="card-text"><small class="text-muted">Fresh</small></p>
												</div>
											</div>
										</div>
									</div>
								</td>
								<td class="border-0 p-0 text-center">12341234</td>
								<td class="border-0 p-0 text-center">$123</td>
								<td class="text-center p-0 text-uppercase border-0 pr-0">Delivery</td>
							</tr>
							<?php 
						endfor;
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</main>
<?php include "./footer.php"; ?>
</html>
</body>
<script> 
	jQuery(document).ready(function($) {
		$('.js-header').addClass('is-page');
	});

	var screen_width = $(window).width(); 
	if (screen_width <= 480) {
		$('html').css('font-size','12px');
	}
</script>

