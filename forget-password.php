<?php include "./header.php"; ?>
<main class="main-sigin creat-account photo-bg" style="background-image: url(./assets/images/account/account-bg.png);">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
			</div>
			<div class="col-md-6">
				<div class="inner">
					<div class="title-general">Forgot Your Password</div>
					<div class="description">If you've forgotten your password, please enter your email address below and click "Submit". We will send you an email within the next few minutes to change your password. <br>Please be aware that for security reasons we will delete any saved credit card information stored with your account when you update your password.</div>
					<div class="signin-form--wrap">
						<form>
							<div class="form-group mb-3">
								<input type="email" class="form-control" placeholder="Enter email">
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include "./footer.php"; ?>
</html>
</body>
<script> 
	jQuery(document).ready(function($) {
		$('.js-header').addClass('is-page');
	});
</script>

