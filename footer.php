<span class="mdi mdi-chevron-up scroll-top d-flex align-items-center justify-content-center rounded-circle border border-dark position-fixed"></span>
<footer>
	<div class="footer">
		<div class="bot_footer">
			<div class="block_items">
				<div class="_logo">
					<img src="assets/images/home_q/logof.png" alt="">
				</div>
			</div>
			<div class="block_items">
				<div class="_item">
					<a href="#" class="_title_sm">MARVEL ART </a>
					<a href="#">About</a>
					<a href="#">Us</a>
					<a href="#">Privacy</a>
					<a href="#">Policy</a>
					<a href="#">Terms & Conditions</a>
					<a href="#">Register</a>
				</div>
				<div class="_item">
					<a href="#" class="_title_sm">POLICY</a>
					<a href="#">Term</a>
					<a href="#">Copyright</a>
					<a href="#">Delivery & Returns</a>
				</div>
				<div class="_item">

					<a href="#" class="_title_sm">SUPPORT</a>
					<a href="#">Store location</a>
					<a href="#">Contact </a>
					<a href="#">FAQs </a>
				</div>
			</div>
			<div class="block_items">
				<div class="_title_sm">CONNECT</div>
				<div class="_wrap_icon">
					<a href=""><i class="mdi mdi-facebook"></i></a>
					<a href=""><i class="mdi mdi-google-plus"></i></a>
					<a href=""><i class="mdi mdi-twitter"></i></a>
					<a href=""><i class="mdi mdi-youtube"></i></a>
				</div>
			</div>
		</div>
	</div>
</footer>
<script> 
	jQuery(document).ready(function($) {	
		$('.scroll-top').click(function() {
			$('html, body').animate({
				scrollTop: 0
			}, 800);
			return false;
		}); 
	});
</script>
</html>
</body>