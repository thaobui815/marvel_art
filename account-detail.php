<?php include "./header.php"; ?>
<main class="main-sigin pd-5 account-detail">
	<section class="page-banner">
		<img src="./assets/images/account/banner-bg.png" height="400" alt="#" class="img-fluid">
        <div class="text-banner position-absolute">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
	</section>
	<div class="container">
		<div class="row m-0">
			<div class="menu-tab col-xl-3 col-md-4">
				<h5 class="pb-2 title-general border-0">Explore Your Account</h5>
				<ul class="nav flex-column">
					<li class="nav-item font-weight-bold">
						<a class="nav-link active" href="#">My Detail</a>
					</li>
					<li class="nav-item font-weight-bold">
						<a class="nav-link " href="#">Favorites</a>
					</li>
					<li class="nav-item font-weight-bold">
						<a class="nav-link" href="#">Order History</a>
					</li>
				</ul>
			</div>
			<div class="input-info col-xl-6 col-md-8">
				<h3 class="pb-3 title-general border-0">Member Information</h3>
				<form>
					<div class="form-group">
						<input type="text" class="form-control rounded-0 shadow-none h-auto" placeholder="First name">
					</div>
					<div class="form-group">
						<input type="text" class="form-control rounded-0 shadow-none h-auto" placeholder="Last name">
					</div>
					<div class="form-group">
						<input type="email" class="form-control rounded-0 shadow-none h-auto" placeholder="Email">
					</div>
					<div class="form-group">
						<input type="text" class="form-control rounded-0 shadow-none h-auto" placeholder="Phone">
					</div>
					<button type="submit" class="btn btn-primary font-weight-bold rounded-0">Save</button>
				</form>
				<h3 class="mt-5 title-general border-0">Delivery Address</h3>
				<form>
					<div class="form-group">
						<input type="text" class="form-control rounded-0 shadow-none h-auto" placeholder="Location name">
					</div>
					<div class="form-group">
						<input type="text" class="form-control rounded-0 shadow-none h-auto" placeholder="Recipient Name">
					</div>
					<div class="form-group">
						<input type="email" class="form-control rounded-0 shadow-none h-auto" placeholder="Company">
					</div>
					<div class="form-group">
						<input type="text" class="form-control rounded-0 shadow-none h-auto" placeholder="Address*">
					</div>
					<div class="form-group">
						<input type="text" class="form-control rounded-0 shadow-none h-auto" placeholder="Suburb*">
					</div>
					<div class="form-group">
						<input type="text" class="form-control rounded-0 shadow-none h-auto" placeholder="City*">
					</div>
					<div class="form-group">
						<input type="text" class="form-control rounded-0 shadow-none h-auto" placeholder="Postcode*">
					</div>
					<button type="submit" class="btn btn-primary font-weight-bold rounded-0">Save</button>
				</form>
			</div>
		</div>
	</div>
</main>
<?php include "./footer.php"; ?>
</html>
</body>
<script> 
	jQuery(document).ready(function($) {
		$('.js-header').addClass('is-page');
	});
</script>