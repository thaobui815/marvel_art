<?php include "./header.php"; ?>
<main class="main-sigin pd-5 favorites">
	<section class="page-banner">
		<img src="https://picsum.photos/1920/400" height="400" alt="#" class="img-fluid">
        <div class="text-banner position-absolute">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
	</section>
	<div class="container pt-5">
		<div class="row mr-md-3 ml-md-0">
			<div class="menu-tab col-xl-3 col-md-4">
				<h5 class="pb-2 title-general border-0">Explore Your Account</h5>
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link" href="#">My Detail</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="#">Favorites</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Order History</a>
					</li>
				</ul>
			</div>
			<div class="input-info col-xl-9 col-md-8">
				<h3 class="pb-3 title-general border-0">Favorites List</h3>
				<div class="row">
					<div class="col-xl-4 col-md-6 mb-5 pr-md-0">
						<div class="card border-0 position-relative">
							<span class="icon-heart mdi mdi-heart position-absolute"></span>
							<a href="" style="background-image: url(https://picsum.photos/900/600)"  class="card-img-top photo-bg w-100 d-inline-block" alt="Card title"></a>
							<div class="card-body">
								<a href=""><h6 class="card-title text-uppercase">wilfred the wale</h6></a>
								<p class="card-text"><span><strong>$59.90</strong></span></p>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-6 mb-5 pr-md-0">
						<div class="card border-0 position-relative">
							<span class="icon-heart mdi mdi-heart position-absolute"></span>
							<a href="" style="background-image: url(https://picsum.photos/900/600)"  class="card-img-top photo-bg w-100 d-inline-block" alt="Card title"></a>
							<div class="card-body">
								<a href=""><h6 class="card-title text-uppercase">wilfred the wale</h6></a>
								<p class="card-text">
									<span style="text-decoration: line-through; font-family: 'Quicksand-regular';">$99.90</span>
									<span class="text-warning"><strong>$59.90</strong></span>
								</p>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-6 mb-5 pr-md-0">
						<div class="card border-0 position-relative">
							<span class="icon-heart mdi mdi-heart position-absolute"></span>
							<a href="" style="background-image: url(https://picsum.photos/900/600)"  class="card-img-top photo-bg w-100 d-inline-block" alt="Card title"></a>
							<div class="card-body">
								<a href=""><h6 class="card-title text-uppercase">wilfred the wale</h6></a>
								<p class="card-text"><span><strong>$59.90</strong></span></p>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-6 mb-5 pr-md-0">
						<div class="card border-0 position-relative">
							<span class="icon-heart mdi mdi-heart position-absolute"></span>
							<a href="" style="background-image: url(https://picsum.photos/900/600)"  class="card-img-top photo-bg w-100 d-inline-block" alt="Card title"></a>
							<div class="card-body">
								<a href=""><h6 class="card-title text-uppercase">wilfred the wale</h6></a>
								<p class="card-text">
									<span style="text-decoration: line-through; font-family: 'Quicksand-regular';">$99.90</span>
									<span class="text-warning"><strong>$59.90</strong></span>
								</p>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-6 mb-5 pr-md-0">
						<div class="card border-0 position-relative">
							<span class="icon-heart mdi mdi-heart position-absolute"></span>
							<a href="" style="background-image: url(https://picsum.photos/900/600)"  class="card-img-top photo-bg w-100 d-inline-block" alt="Card title"></a>
							<div class="card-body">
								<a href=""><h6 class="card-title text-uppercase">wilfred the wale</h6></a>
								<p class="card-text"><span><strong>$59.90</strong></span></p>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-6 mb-5 pr-md-0">
						<div class="card border-0 position-relative">
							<span class="icon-heart mdi mdi-heart position-absolute"></span>
							<a href="" style="background-image: url(https://picsum.photos/900/600)"  class="card-img-top photo-bg w-100 d-inline-block" alt="Card title"></a>
							<div class="card-body">
								<a href=""><h6 class="card-title text-uppercase">wilfred the wale</h6></a>
								<p class="card-text">
									<span class="text-uppercase" style="text-decoration: line-through; font-family: 'Quicksand-regular';">$99.90</span>
									<span class="text-warning"><strong>$59.90</strong></span>
								</p>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-6 mb-5 pr-md-0">
						<div class="card border-0 position-relative">
							<span class="icon-heart mdi mdi-heart position-absolute"></span>
							<a href="" style="background-image: url(https://picsum.photos/900/600)"  class="card-img-top photo-bg w-100 d-inline-block" alt="Card title"></a>
							<div class="card-body">
								<a href=""><h6 class="card-title text-uppercase">wilfred the wale</h6></a>
								<p class="card-text"><span><strong>$59.90</strong></span></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include "./footer.php"; ?>
</html>
</body>
<script> 
	jQuery(document).ready(function($) {
		$('.js-header').addClass('is-page');
	});
	// var screen_width = $(window).width(); 
	// if (screen_width <= 1440) {
	// 	$('html').css('font-size','12px');
	// }
</script>

