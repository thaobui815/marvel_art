<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Marvel Art</title>
	<link rel="shortcut icon" href="./assets/images/favicon.png" type="image/x-icon"/>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
	<link rel="stylesheet" href="//cdn.materialdesignicons.com/4.4.95/css/materialdesignicons.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<link rel="stylesheet" href="./assets/bower_components/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" href="./assets/bower_components/Swiper/package/css/swiper.min.css">  
	<link rel="stylesheet" href="./assets/bower_components/fancybox/dist/jquery.fancybox.min.css">
	<link rel="stylesheet" href="./assets/bower_components/aos/dist/aos.css">
	<link rel="stylesheet" href="./assets/stylesheets/sass/dist/main.css">
	<link rel="stylesheet" href="./assets/stylesheets/sass/dist/partials.css">

	<script src="http://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<script src="./assets/bower_components/Swiper/package/js/swiper.min.js" type="text/javascript"></script>
	<script src="./assets/bower_components/fancybox/dist/jquery.fancybox.min.js" type="text/javascript"></script>
	<script src="./assets/bower_components/aos/dist/aos.js" type="text/javascript"></script>
	<script src="./assets/scripts/main.js" type="text/javascript"></script>
</head>
<body>
	<header class="js-header">
		<a href="index.php" class="logo-header"><img src="./assets/images/homepage/logo.png" alt=""></a>
		<div class="d-flex align-items-center">
			<div class="form-search">
				<button id="search"><i class="material-icons">search</i></button>
				<input type="text" placeholder="Search">
			</div>
			<ul class="menu-page mr-5 ml-4">
				<li>
					<a href="#" class="menu-item">home</a>
				</li>
				<li><a href="#" class="menu-item">shop</a></li>
				<li>
					<a href="#" class="menu-item">product</a>
					<ul class="sub-menu p-0">
						<li>
							<a href="#" class="menu-item">Sản phẩm 1</a>
						</li>
						<li>
							<a href="#" class="menu-item">Sản phẩm 2</a>
						</li>
						<li>
							<a href="#" class="menu-item">Sản phẩm 3</a>
						</li>
					</ul>
				</li>
				<li><a href="#" class="menu-item">cart</a></li>
				<li><a href="#" class="menu-item">checkout</a></li>
			</ul>
			<ul class="sign-shop">
				<li class="position-relative">
					<a href="#" id="user--header"><i class="material-icons">person</i></a>
					<div class="dropdown-user">
						<a href="#">Sign in</a>
						<a href="#">Create an account</a>
					</div>
				</li>
				<li>
					<a href="#" class="d-flex align-items-center">
						<i class="material-icons">shopping_cart</i>
						<span class="badge badge-light ml-2">2</span>
					</a>
				</li>
			</ul>
		</div>
	</header>
	<div class="button-menu"><span></span></div>
	<script> 
		jQuery(document).ready(function($) {
			$('#search').click(function(event) {
				$(this).parent().toggleClass('click--in');
			});
			$('.button-menu').click(function(event) {
				$(this).toggleClass('click--in');
				$('header').toggleClass('click--in');
			});
			$(window).scroll(function () {
				var y = $(window).scrollTop();
				if (y > 100) {
					$('header').addClass('scroll');
				}
				else{
					$('header').removeClass('scroll');
				}
			});

			$(window).resize(function(){
				var width = $(window).width();
				if (width <= 1440){
				}
			});


			var screen_width = $(window).width(); 
			if (screen_width <= 768) {
				$('.sub-menu').slideUp();
				$('header .menu-page li').click(function(event) {
					$(this).children('.sub-menu').slideToggle();
				});
			}
		});
	</script>

